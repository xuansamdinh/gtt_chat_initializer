ShowAboutAndHelp:
    Gui AboutAndHelp: New, LabelAboutAndHelp -MinimizeBox
    Gui Color, White
    Gui Add, Picture, x9 y10 w64 h64, gtt_chat_initializer_icon.png
    Gui Font, s20 W700 Q4 c00ADEF, Verdana
    Gui Add, Text, x80 y10 w400, GTT Chat Initializer
    Gui Font
    Gui Font, s10, Consolas
    Gui Add, Text, x81 y46 w365 h16, v%AppVersion%
    Gui Font
    Gui Font, s10, Segoe UI
    Gui Add, Link, x81 y74 w200 h16, Source: <a href="https://bitbucket.org/xuansamdinh/gtt_chat_initializer">BitBucket Project Page.</a>
    Gui Add, Link, x81 y94 w200 h16, Author: <a href="mailto:Xuan_Sam_Dinh@dell.com?subject=GTT Chat Initializer Feedback">Xuan Sam Dinh {at} Dell.</a>
    Gui Add, Link, x81 y124 w240 h16, Read the doc in a <a href="help\gtt_chat_initializer_help.pdf">PDF</a> or <a href="help\gtt_chat_initializer_help.html">HTML</a> file.
    Gui Add, Link, x81 y144 w240 h16, <a href="https://bitbucket.org/xuansamdinh/gtt_chat_initializer/downloads/">Download the latest version.</a>
    Gui Add, Link, x81 y164 w240 h16, <a href="https://bitbucket.org/xuansamdinh/gtt_chat_initializer/addon/bucketboard/board">Bug report. Feature request.</a>
    Gui Add, Text, x0 y209 w463 h1 +0x5
    Gui Add, Text, x0 y210 w463 h48 -Background
    Gui Add, Text, x16 y225 w365 +0x4000 -Background, % "AutoHotkey " . A_AhkVersion . " " . (A_IsUnicode ? "Unicode" : "ANSI") . " " . (A_PtrSize == 8 ? "64-bit" : "32-bit")
    Gui Add, Button, gAboutAndHelpClose x320 y225 w80 h24 Default, &OK
    Gui Show, w430 h260, About
    ControlFocus Button1, About
    Gui +LastFound
    SendMessage 0x80, 0, DllCall("LoadIcon", "Ptr", 0, "Ptr", 32516, "Ptr")
Return

AboutAndHelpEscape:
AboutAndHelpClose:
    Gui AboutAndHelp: Destroy
Return
