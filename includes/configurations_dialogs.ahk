﻿ConfigurationTool:

;==============================================================================

    LoadSettings()

;==============================================================================

    Gui ConfigurationDialog: New, +LabelConfigurationDialog_On +hWndhConfigurationDialog -Resize -MinimizeBox -MaximizeBox +OwnDialogs
    Gui Font, s12 cNavy, Segoe UI
    Gui Add, Text, x140 y5 w240 h24 +0x200 +Center -Background, Configuration Items
    Gui Font
    Gui Font, s10, Segoe UI
    Gui Add, Text, x10 y35 w490 h20, Note: Currently supports APJ, TS, Penang only. Want to add more?
    Gui Font
    Gui Font, s10, Segoe UI
    Gui Add, Edit, vuserName x10 y80 w240 h24, %userName%
    Gui Add, Edit, vuserBadge x10 y120 w240 h24, %userBadge%
    Gui Add, Edit, vuserEmail x10 y160 w240 h24, %userEmail%
    if (AutoEnterChat)
        Gui Add, Checkbox, vAutoEnterChat Checked x260 y56 w240 h24, &Auto Enter Chat?
    else
        Gui Add, Checkbox, vAutoEnterChat x260 y56 w240 h24, &Auto Enter Chat?
    if (MaximizeBrowserWindow)
        Gui Add, Checkbox, vMaximizeBrowserWindow Checked x260 y84 w240 h24, &Maximize browser window?
    else
        Gui Add, Checkbox, vMaximizeBrowserWindow x260 y84 w240 h24, &Maximize browser window?
    Gui Add, Edit, vgttUpdateNBDMesg x10 y200 w240 h60, %gttUpdateNBDMesg%
    Gui Add, Edit, vgttCorrectWarrantyMesg x260 y120 w240 h65, %gttCorrectWarrantyMesg%
    Gui Add, Edit, vgttPOPUpdateMesg x260 y200 w240 h60, %gttPOPUpdateMesg%
    Gui Font
    Gui Font, s11, Segoe UI
    Gui Add, Button, gSaveSettings x40 y280 w160 h30, &OK
    Gui Add, Button, gCancelSettingChange x300 y280 w160 h30, &Cancel

    Gui Show, w510 h320, Configurations

Return

;==============================================================================

CancelSettingChange:
    Gui ConfigurationDialog: Destroy
Return

;==============================================================================

SaveSettings:
    GuiControlGet, userName,, userName
    GuiControlGet, userBadge,, userBadge
    GuiControlGet, userEmail,, userEmail
    GuiControlGet, gttUpdateNBDMesg,, gttUpdateNBDMesg
    GuiControlGet, gttCorrectWarrantyMesg,, gttCorrectWarrantyMesg
    GuiControlGet, gttPOPUpdateMesg,, gttPOPUpdateMesg
    GuiControlGet, AutoEnterChat,, AutoEnterChat
    GuiControlGet, MaximizeBrowserWindow,, MaximizeBrowserWindow

    Gui ConfigurationDialog: Destroy

    SaveSettings()
    LoadSettings()

    GuiControl, GttChat:, AutoEnterChatCheckBox, %AutoEnterChat%
    GuiControl, GttChat:, MaximizeBrowserWindowCheckbox, %MaximizeBrowserWindow%

Return
