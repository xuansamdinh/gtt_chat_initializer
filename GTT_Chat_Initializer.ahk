﻿#SingleInstance Force
#NoTrayIcon
#NoEnv
#Warn
SetWorkingDir %A_ScriptDir%
SetBatchLines -1
FileEncoding UTF-8

;==============================================================================

Global GttMoxieURL := "http://channels.us.dell.com/netagent/cimlogin.aspx?questid=C7DDDEC1-107B-4A16-9B90-0567927D3922&portid=087ABB65-9138-43E2-B18E-35A835D06072"
, IniConfigFile := A_ScriptDir . "\gtt_chat_initializer.ini"
, AppVersion := "1.6.3"
, userName
, userEmail
, userBadge
, gttUpdateNBDMesg
, gttCorrectWarrantyMesg
, gttPOPUpdateMesg
, AutoEnterChat
, MaximizeBrowserWindow

;==============================================================================

LoadSettings()

;==============================================================================
Gui GttChat: New, +LabelGttChatDialog_On +hWndGttChatDialog -Resize -MinimizeBox -MaximizeBox +OwnDialogs -AlwaysOnTop
Gui Font, s10, Segoe UI
Gui Add, Text, x10 y10 w120 h24 +0x1 +0x200 Left, SERVICE TAG
Gui Add, Text, x10 y50 w120 h24 +0x1 +0x200 Left, SERVICE REQUEST
Gui Add, Text, x10 y90 w120 h24 +0x1 +0x200 Left, COMPANY
Gui Add, Edit, hWndhEdit1 vServiceTag x150 y10 w140 h24
DllCall("SendMessage", "Ptr", hEdit1, "UInt", 0x1501, "Ptr", 1, "WStr", "Service Tag") ; Hint text
Gui Add, Edit, hWndhEdit2 vServiceRequest x150 y50 w140 h24 +Number
DllCall("SendMessage", "Ptr", hEdit2, "UInt", 0x1501, "Ptr", 1, "WStr", "Service Request") ; Hint text
Gui Add, Edit, hWndhEdit3 vCompanyName x150 y90 w140 h24
DllCall("SendMessage", "Ptr", hEdit3, "UInt", 0x1501, "Ptr", 1, "WStr", "Company Name") ; Hint text
Gui Add, Radio, vGttChatTypeWarranty Checked x10 y120 w120 h24, P&eriods update?
Gui Add, Radio, vGttChatTypeNBD x10 y140 w120 h24, &NBD update?
Gui Add, Radio, vGttChatTypePOP x10 y160 w120 h24, &POP update?
if (AutoEnterChat)
    Gui Add, CheckBox, vAutoEnterChatCheckBox gUpdateAutoEnterChat Checked x150 y120 w120 h24, &Auto Enter Chat?
else
    Gui Add, CheckBox, vAutoEnterChatCheckBox gUpdateAutoEnterChat x150 y120 w120 h24, &Auto Enter Chat?
if (MaximizeBrowserWindow)
    Gui Add, CheckBox, vMaximizeBrowserWindowCheckBox gUpdateMaximizeBrowserWindow Checked x150 y140 w130 h24, &Maximize Window?
else
    Gui Add, CheckBox, vMaximizeBrowserWindowCheckBox gUpdateMaximizeBrowserWindow x150 y140 w130 h24, &Maximize Window?
Gui Font,
Gui Font, s20, Segoe UI
Gui Add, Button, hWndokButton vokButtonVar gChatGTT x150 y170 w140 h44, &Chat
Gui Font,
Gui Font, s10, Segoe UI
Gui Add, Button, hWndConfigurationButton vConfigurationButtonVar gConfigurationTool x10 y190 w100 h24, Con&figurations
Gui Add, Button, hWndAboutAndHelpButton vShowAboutAndHelpButtonVar gShowAboutAndHelp x115 y190 w30 h24, &?

Gui Show, w300 h230, GTT Chat Initializer v%AppVersion%

Return

;==============================================================================

#Include, %A_ScriptDir%\includes\configurations_dialogs.ahk
#Include, %A_ScriptDir%\includes\about_help.ahk

;==============================================================================

GuiEscape:
GuiClose:
GttChatDialog_OnClose:
    ExitApp

Esc::ExitApp

;==============================================================================

ChatGTT:
    GuiControlGet, ServiceTag,, ServiceTag
    GuiControlGet, ServiceRequest,, ServiceRequest
    GuiControlGet, CompanyName,, CompanyName
    GuiControlGet, GttChatTypeWarrantyValue,, GttChatTypeWarranty
    GuiControlGet, GttChatTypeNBDValue,, GttChatTypeNBD
    GuiControlGet, GttChatTypePOPValue,, GttChatTypePOP
    GuiControlGet, AutoEnterChatCheckBox,, AutoEnterChatCheckBox
    GuiControlGet, MaximizeBrowserWindowCheckBox,, MaximizeBrowserWindowCheckBox

    if (ServiceTag = "" or ServiceRequest = "" or CompanyName = "")
    {
        MsgBox, 16, Alert, Both Service Tag, SR and`nCompany Name are requested.
        Return
    }

    Gui GttChat: Destroy

    IEBrowser := ComObjCreate("{D5E8041D-920F-45e9-B8FB-B1DEB82C6E5E}")
    IEBrowser.Visible := true
    IEBrowser.Navigate(GttMoxieURL)

    if (MaximizeBrowserWindowCheckBox) {
        WinMaximize, % "ahk_id " IEBrowser.HWND
    }

    while IEBrowser.Busy
        Continue

    ; Let browser to load the page completely first
    Sleep 1000

    IEBrowser.document.getElementByID("LoginName").Value := userName
    IEBrowser.document.getElementByID("EMAIL").Value := userEmail
    IEBrowser.document.getElementByID("Badge").Value := userBadge

    IEBrowser.document.getElementByID("Company_Name").Value := CompanyName
    IEBrowser.document.getElementByID("Service_Tag").Value := ServiceTag
    IEBrowser.document.getElementByID("ServiceRequestID").Value := ServiceRequest

    IEBrowser.document.getElementByID("IssueType").Focus()

    if (GttChatTypePOPValue or GttChatTypeWarrantyValue)
        IEBrowser.document.getElementByID("IssueType").value := "Warranty_Date_Change"
    else
        IEBrowser.document.getElementByID("IssueType").value := "Dispatch_issue"

    IEBrowser.document.getElementByID("RouteIdent").Focus()
    IEBrowser.document.getElementByID("RouteIdent").Value := "IN.GTT.Global"

    IEBrowser.document.getElementByID("Your_Site_Location").Focus()
    IEBrowser.document.getElementByID("Your_Site_Location").Value := "Penang"

    IEBrowser.document.getElementByID("Support_Role").Focus()
    IEBrowser.document.getElementByID("Support_Role").Value := "Technical_Support"

    IEBrowser.document.getElementByID("Support_Region").Focus()
    IEBrowser.document.getElementByID("Support_Region").Value := "Asia_Pacific"

    if (GttChatTypeWarrantyValue)
        IEBrowser.document.getElementByID("Subject").Value := gttCorrectWarrantyMesg
    else if (GttChatTypePOPValue)
        IEBrowser.document.getElementByID("Subject").Value := gttPOPUpdateMesg
    else
        IEBrowser.document.getElementByID("Subject").Value := gttUpdateNBDMesg

    IEBrowser.document.getElementByID("submitBtn").Focus()

    if (AutoEnterChatCheckBox)
    {
        IEBrowser.document.getElementByID("submitBtn").Click()
    }

    ExitApp
Return

UpdateAutoEnterChat:
    GuiControlGet, AutoEnterChat,, AutoEnterChatCheckBox

    SaveSettings()
Return

UpdateMaximizeBrowserWindow:
    GuiControlGet, MaximizeBrowserWindow,, MaximizeBrowserWindowCheckBox

    SaveSettings()
Return

;==============================================================================

LoadSettings() {
    IniRead, userName, %IniConfigFile%, UserInfo, userName, A_User_Name
    IniRead, userEmail, %IniConfigFile%, UserInfo, userEmail, A_User_Name@dell.com
    IniRead, userBadge, %IniConfigFile%, UserInfo, userBadge, 1234567
    IniRead, gttUpdateNBDMesg, %IniConfigFile%, AppSettings, gttUpdateNBDMesg, "Update NBD."
    IniRead, gttCorrectWarrantyMesg, %IniConfigFile%, AppSettings, gttCorrectWarrantyMesg, "Correct arranty periods"
    IniRead, gttPOPUpdateMesg, %IniConfigFile%, AppSettings, gttPOPUpdateMesg, "POP update."

    IniRead, AutoEnterChat, %IniConfigFile%, AppSettings, AutoEnterChat, 1
    IniRead, MaximizeBrowserWindow, %IniConfigFile%, AppSettings, MaximizeBrowserWindow, 1
}

SaveSettings() {
    if (!FileExist(IniConfigFile)) {
        Sections := "[UserInfo]`n`n[AppSettings]`n"
        FileAppend, %Sections%, %IniConfigFile%, UTF-8
    }

    IniWrite, %userName%, %IniConfigFile%, UserInfo, userName
    IniWrite, %userEmail%, %IniConfigFile%, UserInfo, userEmail
    IniWrite, %userBadge%, %IniConfigFile%, UserInfo, userBadge
    IniWrite, %gttUpdateNBDMesg%, %IniConfigFile%, AppSettings, gttUpdateNBDMesg
    IniWrite, %gttCorrectWarrantyMesg%, %IniConfigFile%, AppSettings, gttCorrectWarrantyMesg
    IniWrite, %gttPOPUpdateMesg%, %IniConfigFile%, AppSettings, gttPOPUpdateMesg
    IniWrite, %AutoEnterChat%, %IniConfigFile%, AppSettings, AutoEnterChat
    IniWrite, %MaximizeBrowserWindow%, %IniConfigFile%, AppSettings, MaximizeBrowserWindow
}

;==============================================================================
