# GTT Chat Initializer - Help

[TOC]

![gtt_chat_initializer_overview](assets/gtt_chat_initializer_overview.png)

## Install

- Download
- Extract
- Compiling

## Configuration

- Variables
- Loading, Saving

## Usage

- Input field
- Auto Enter Chat?

## Getting Help

- The project page

